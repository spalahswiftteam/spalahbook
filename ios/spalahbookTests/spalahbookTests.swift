//
//  spalahbookTests.swift
//  spalahbookTests
//
//  Created by Michael on 8/2/18.
//  Copyright © 2018 Mission Edition. All rights reserved.
//

import XCTest

@testable import spalahbook

class spalahbookTests: XCTestCase {
    
    var presenterUnderTest: MainPresenter!
    var view: MockView!
    
    override func setUp() {
        super.setUp()
        view = MockView()
        let service = MockWallService()
        presenterUnderTest = MainPresenter(service: service)
        presenterUnderTest.attach(view: view)
    }
    
    override func tearDown() {
        presenterUnderTest = nil
        super.tearDown()
    }
    
    func testPresenter() {
        let future = expectation(description: "Finish loading")
        
        view.didFinish = {
            future.fulfill()
        }
        
        presenterUnderTest.loadPosts()
        waitForExpectations(timeout: 1, handler: nil)
        
        XCTAssertEqual(presenterUnderTest.posts.count, 5)
    }
}
