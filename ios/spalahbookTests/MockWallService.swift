//
//  MockWallService.swift
//  spalahbook
//
//  Created by Michael on 8/2/18.
//  Copyright © 2018 Mission Edition. All rights reserved.
//

import Foundation

@testable import spalahbook

class MockWallService: WallServiceProtocol {
    func getPublications(id: String, offset: Int, quantity: Int, completion: @escaping ([Location]?, Error?) -> Void) {
        guard let path = Bundle.main.path(forResource: "testResponse", ofType: "json") else { fatalError() }
        let url = URL(fileURLWithPath: path)
        guard let data = try? Data(contentsOf: url) else { fatalError() }
        guard let locations = try? JSONDecoder().decode([Location].self, from: data) else { fatalError() }
        completion(locations, nil)
    }
}
