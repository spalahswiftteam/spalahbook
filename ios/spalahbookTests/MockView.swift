//
//  MockView.swift
//  spalahbookTests
//
//  Created by Michael on 8/2/18.
//  Copyright © 2018 Mission Edition. All rights reserved.
//

@testable import spalahbook

class MockView: MainView {
    var didFinish: (() -> Void)?
    
    func didLoad(posts: [Location]) {
        didFinish?()
    }
    
    func handle(error: Error) {
        didFinish?()
    }
}
