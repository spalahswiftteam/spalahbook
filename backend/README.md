# Methods

NOTE: To get details of current user use 'self' for an id.

---

## Registration

`/register`

### POST

BODY
```swift
{
	"name": String,
	"description": String, // optional
	"email": String,
	"password": String,
	"image": String // optional
}
```

#####RESPONSE
```swift
{
	"key": String
}
```

---

## Authorization

`/auth`

### POST

BODY
```swift
{
	"email": String,
	"password": String
}
```

#####RESPONSE
```swift
{
	"key": String
}
```

---

## Profile

`/profile`

### GET

HEADERS
```swift
	"Key": String
```

PARAMETERS
```swift
	"id": String
```

#####RESPONSE
```swift
{
    "id": String,
	"name": String,
	"description": String,
	"image": String
}
```

---

## Search

`/search`

### GET

HEADERS
```swift
"Key": String
```

PARAMETERS
```swift
"q": String
```

#####RESPONSE
```swift
[{
    "id": String,
    "name": String,
    "description": String,
    "image": String
}, ...]
```

---

## Subscriptions

`/subs`

### GET

HEADERS
```swift
"Key": String
```

#####RESPONSE
```swift
[{
    "id": String,
    "name": String,
    "description": String,
    "image": String
}, ...]
```

### POST

HEADERS
```swift
"Key": String
```

PARAMETERS
```swift
"id": String
```

### DELETE

HEADERS
```swift
"Key": String
```

PARAMETERS
```swift
"id": String
```

---

## Wall

`/wall`

### GET

HEADERS
```swift
"Key": String
```

PARAMETERS
```swift
"id": String
"offset": Int
"quantity": Int
```

#####RESPONSE
```swift
[{
    "lat": Float,
    "lng": Float,
    "description": String,
    "date": Float
}, 
...]
```

### POST

HEADERS
```swift
"Key": String
```

BODY
```swift
{
    "lat": Float,
    "lng": Float,
    "description": String // optional
}
```

### DELETE

HEADERS
```swift
"Key": String
```

PARAMETERS
```swift
"time": Float
```

---

## Media

`/media`

### GET

HEADERS
```swift
	"Key": String
```

PARAMETERS
```swift
	"id": String
```

#####RESPONSE
```swift
{
	"data": base64 encoded string
}
```

### POST

BODY
```swift
{
	"data": base64 encoded string
}
```

#####RESPONSE
```swift
{
	"id": String
}
```

---

# Error response
```swift
{
	"error": {
		"code": Int,
		"message": String
	}
}
```

# Warning response
```swift
{
    "warning": {
        "code": Int,
        "message": String
    }
}
```
