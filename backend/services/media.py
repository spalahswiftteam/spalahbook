#!/usr/bin/env python3

import webapp2
import json

from utilities.utilities import error

from models.data import Data
from models.user import User

class Media(webapp2.RequestHandler):
	def get(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "Unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "Invalid key"))
			return

		media_id = self.request.get("id")
		if not media_id:
			self.response.write(error(3, "id is required"))
			return

		try:
			media_id = int(media_id)
		except:
			self.response.write(error(4, "Invalid id"))
			return

		data = Data.retrive(media_id)
		response_dict = { Data.ApiKeys.DATA: data }
		self.response.write(json.dumps(response_dict))

	def post(self):
		try:
			body = json.loads(self.request.body)
		except:
			self.response.write(error(1, "Invalid json"))
			return

		data = body.get("data")
		if not data:
			self.response.write(error(2, "No data provided"))
			return

		media_id = Data.create(data)
		response_dict = { Data.ApiKeys.ID: media_id }
		self.response.write(json.dumps(response_dict))

	def delete(self):
		Data.drop()
