#!/usr/bin/env python3

import webapp2
import json

from utilities.utilities import error
from models.user import User

class Auth(webapp2.RequestHandler):
	def post(self):
		try:
			body = json.loads(self.request.body)
		except:
			self.response.write(error(1, "Bad json"))
			return

		email = body.get("email")
		password = body.get("password")
		person = User.auth(email, password)

		if person:
			self.response.write(person.json(person.access_hash[-1], only_key = True))
		else:
			self.response.write(error(2, "Check credentials"))