#!/usr/bin/env python3

import webapp2
import json

from utilities.utilities import error
from models.user import User

class Register(webapp2.RequestHandler):
	def post(self):
		try:
			body = json.loads(self.request.body)
		except:
			self.response.write(error(1, "Bad json"))
			return

		email = body.get("email")
		password = body.get("password")
		name = body.get("name")
		if not email or not password or not name:
			self.response.write(error(1, "email, password and name are required"))
			return

		description = body.get("description")
		image = body.get("image")
		person = User.create(email, password, name, description, image)

		if person:
			self.response.write(person.json(person.access_hash[-1], only_key = True))
		else:
			self.response.write(error(2, "User exists"))

	def delete(self):
		User.drop()

