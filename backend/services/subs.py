#!/usr/bin/env python3

import webapp2
import json

from utilities.utilities import error, warning
from models.user import User

class Subs(webapp2.RequestHandler):
	def get(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "Unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "Invalid key"))
			return

		self.response.write(person.get_subscriptions_json())

	def post(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "Unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "Invalid key"))
			return

		user_id = self.request.params.get("id")
		if not user_id:
			self.response.write(error(3, "id required"))
			return
		
		user = User.userById(user_id)
		if not user:
			self.response.write(error(4, "No user found"))
			return

		person.subscribe(user)

	def delete(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "Unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "Invalid key"))
			return

		user_id = self.request.params.get("id")
		if not user_id:
			self.response.write(error(3, "id required"))
			return

		user = User.userById(user_id)
		if not user:
			self.response.write(error(4, "No user found"))
			return

		person.unsubscribe(user)
