#!/usr/bin/env python3

import webapp2
import json

from utilities.utilities import error, warning
from models.user import User

class Search(webapp2.RequestHandler):
	def get(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "Unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "Invalid key"))
			return

		q = self.request.params.get("q")
		if not q:
			self.response.write(error(3, "search query is required"))
			return

		offset = self.request.params.get("offset")
		if not offset:
			offset = 0

		quantity = self.request.params.get("quantity")
		if not quantity:
			quantity = 10

		try:
			offset = int(offset)
			quantity = int(quantity)
		except:
			self.response.write(error(4, "offset and quantity should be integers"))
			return

		if quantity <= 0:
			self.response.write(error(5, "quantity should be greater than zero"))
			return


		users = User.search(q, offset, quantity)
		response = [user.dict() for user in users]
		self.response.write(json.dumps(response))
