#!/usr/bin/env python3

import webapp2
import json

from utilities.utilities import error, warning, dumps

from models.user import User
from models.location import Location


class Wall(webapp2.RequestHandler):
	def get(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "Unauthorized"))
			return

		user_id = self.request.params.get("id")
		offset = self.request.params.get("offset")
		quantity = self.request.params.get("quantity")

		if not user_id and not quantity or not quantity:
			self.response.write(error(2, "id, offset and quantity are required"))
			return

		try:
			offset = int(offset)
			quantity = int(quantity)
		except:
			self.response.write(error(3, "offset and quantity should be integers"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(4, "invalid key"))
			return
			
		if user_id == "self":
			locations = person.get_locations(offset, quantity)
		else:
			user = User.userById(user_id)
			if not user:
				self.response.write(error(5, "user with such id does not exist"))
				return
			locations = user.get_locations(offset, quantity)

		locations = [location.dict for location in locations]
		self.response.write(dumps(locations))



	def post(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "check credentials"))
			return

		try:
			body = json.loads(self.request.body)
		except:
			self.response.write(error(3, "bad json"))
			return

		lat = body.get(Location.ApiKeys.lat)
		lng = body.get(Location.ApiKeys.lng)
		if not lat or not lng:
			self.response.write(error(4, "invalid location"))
			return

		location = Location.create(lat, lng, person)
		description = body.get(Location.ApiKeys.description)
		if description:
			location.description = description

		location.post()

	def delete(self):
		key = self.request.headers.get("Key")
		if not key:
			self.response.write(error(1, "unauthorized"))
			return

		person = User.userByHash(key)
		if not person:
			self.response.write(error(2, "check credentials"))
			return

		post_time = self.request.params.get("time")
		if not post_time:
			self.response.write(error(3, "time parameter is required"))
			return

		try:
			post_time = float(post_time)
		except:
			self.response.write(error(4, "time should be a float"))
			return

		person.delete_location(post_time)
