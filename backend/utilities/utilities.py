#!/usr/bin/env python3

import json
import hashlib

def make_hash(content):
	return hashlib.sha256(content).hexdigest()

def dumps(object):
	return json.dumps(object)

def error(code, message):
	return dumps({"error": {"code": code, "message": message}})

def warning(code, message):
	return dumps({"warning": {"code": code, "message": message}})
