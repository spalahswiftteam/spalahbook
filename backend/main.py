#!/usr/bin/env python3

import webapp2

from services.ping import Ping
from services.profile import Profile
from services.media import Media
from services.register import Register
from services.auth import Auth
from services.wall import Wall
from services.subs import Subs
from services.search import Search

app = webapp2.WSGIApplication([
	('/', Ping),
	('/profile', Profile),
	('/media', Media),
	('/register', Register),
	('/auth', Auth),
	('/wall', Wall),
	('/subs', Subs),
	('/search', Search)
], debug=True)
