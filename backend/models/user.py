#!/usr/bin/env python3

from google.appengine.ext import ndb
import time

from utilities.utilities import dumps, make_hash
from location import Location

class User(ndb.Model):
	class ApiKeys:
		email = "email"
		name = "name"
		description = "description"
		image = "image"
		key = "key"
		user_id = "id"

	email = ndb.StringProperty('e')
	access_hash = ndb.StringProperty('a', repeated = True)
	name = ndb.StringProperty('n', default = "")
	description = ndb.StringProperty('d', default = "")
	image = ndb.StringProperty('i', default = "")
	subscriptions = ndb.KeyProperty('s', repeated = True)
	user_id = ndb.StringProperty('u')

	@staticmethod
	def create_key(email):
		return ndb.Key(User, email)

	@staticmethod
	def userByHash(user_hash):
		users = User.query(User.access_hash == user_hash).fetch(1)
		if len(users) > 0:
			return users[0]

		return None

	@staticmethod
	def userById(user_id):
		users = User.query(User.user_id == user_id).fetch(1)
		if len(users) > 0:
			return users[0]

		return None

	@staticmethod
	def create(email, password, name, description, image):
		user_key = User.create_key(email)
		person = user_key.get()
		if person:
			return None

		person = User(key=user_key)
		person.email = email
		timestamp = time.time()
		person_hash = make_hash(email + password + str(timestamp))
		person.access_hash = [person_hash]
		person.name = name
		person.user_id = make_hash(email)
		if description:
			person.description = description

		if image:
			person.image = image

		person.put()
		return person

	@staticmethod
	def auth(email, password):
		user_key = User.create_key(email)
		person = user_key.get()
		if not person:
			return None

		timestamp = time.time()
		person_hash = make_hash(email + password + str(timestamp))
		person.access_hash.append(person_hash)
		person.put()
		return person

	@staticmethod
	def search(q, offset, quantity):
		users = User.query( User.name == q ).fetch()
		if len(users) > offset + quantity:
			return users[offset:offset + quantity]

		q_split = q.split()
		all_users = User.query().order( User.name )
		cursor = 0
		batch_size = 100
		while True:
			batch = all_users.fetch(batch_size, offset = cursor)
			for user in batch:
				if user in users:
					continue

				fits = True
				for param in q_split:
					if not param in user.name:
						fits = False
						break

				if fits:
					users.append(user)

			if len(users) > offset + quantity or len(batch) == 0:
				break

			cursor += batch_size
		
		if len(users) > offset:
			return users[offset:offset + quantity]

		return []

	@staticmethod
	def drop():
		ndb.delete_multi( User.query().fetch(keys_only=True) )

	def dict(self, key = None, only_key = False, private = False):
		user_dict = {}
		if not only_key:
			if private:
				user_dict[User.ApiKeys.email] = self.email
			user_dict[User.ApiKeys.name] = self.name
			user_dict[User.ApiKeys.description] = self.description
			user_dict[User.ApiKeys.image] = self.image
			user_dict[User.ApiKeys.user_id] = self.user_id
		if key:
			user_dict[User.ApiKeys.key] = str(key)

		return user_dict

	def json(self, key = None, only_key = False, private = False):
		return dumps(self.dict(key, only_key, private))

	def subscribe(self, user):
		user_key = user.key
		if not user_key in self.subscriptions:
			self.subscriptions.append(user_key)
			self.put()

	def unsubscribe(self, user):
		user_key = user.key
		if user_key in self.subscriptions:
			self.subscriptions.remove(user_key)
			self.put()

	def get_subscriptions_json(self):
		subs = ndb.get_multi( self.subscriptions )
		subs_arr = []
		for user in subs:
			subs_arr.append(user.dict())

		return dumps(subs_arr)

	def delete_location(self, time):
		locations = Location.query( Location.date == time ).fetch()
		if len(locations) > 0:
			location = locations[0]
			location.key.delete()

	def get_locations(self, offset, quantity):
		return Location.query( Location.author == self.key ).order( -Location.date ).fetch(quantity, offset = offset)
		