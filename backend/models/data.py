#!/usr/bin/env python3

from google.appengine.ext import ndb
import hashlib
import time

class Data(ndb.Model):
	class ApiKeys:
		ID = "id"
		DATA = "data"

	data = ndb.StringProperty(repeated=True)

	@staticmethod
	def create(data):
		media = Data()
		length = int(1000)
		data_len = len(data)
		count = int(data_len / length)
		for i in range(count):
			location = i * length
			chunk = data[location : location + length]
			media.data.append(chunk)

		location = count * length
		remains = data_len - location
		chunk = data[location : location + remains]
		media.data.append(chunk)
		media.put()
		return media.key.id()

	@staticmethod
	def retrive(key):
		key = ndb.Key(Data, key)
		media = key.get()

		result = ""
		for chunk in media.data:
			result += chunk

		return result


	@staticmethod
	def drop():
		ndb.delete_multi( Data.query().fetch(keys_only=True) )
