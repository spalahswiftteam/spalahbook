#!/usr/bin/env python3

from google.appengine.ext import ndb
import time

class Location(ndb.Model):
	class ApiKeys:
		lat = "lat"
		lng = "lng"
		description = "description"
		date = "date"

	lat = ndb.FloatProperty('lt')
	lng = ndb.FloatProperty('lg')
	author = ndb.KeyProperty('a')
	description = ndb.StringProperty('d', default = "")
	date = ndb.FloatProperty()

	@staticmethod
	def create(lat, lng, author, description = ""):
		location = Location()

		location.lat = lat
		location.lng = lng
		location.description = description
		location.author = author.key
		timestamp = time.time()
		location.date = timestamp

		return location

	def post(self):
		return self.put()

	@property
	def dict(self):
		location_dict = {Location.ApiKeys.lat: self.lat,
						Location.ApiKeys.lng: self.lng,
						Location.ApiKeys.description: self.description,
						Location.ApiKeys.date: self.date}

		return location_dict